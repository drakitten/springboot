package kz.aitu.springBoot.repository;

import kz.aitu.springBoot.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    List<Student> findAllByGroupId(long group_id);
}
