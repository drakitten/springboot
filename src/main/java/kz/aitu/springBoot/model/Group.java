package kz.aitu.springBoot.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Group {
    private long id;
    private String name;
}
